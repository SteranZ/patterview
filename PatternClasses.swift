//
//  Rectangle.swift
//  pentagrama
//
//  Created by Sergio Teran on 5/28/16.
//  Copyright © 2016 Sergio Teran. All rights reserved.
//

import Foundation
import UIKit

class PatternFrame: UIView {
    
    // Load the Pattern
    let SinglePattern = Pattern(fileContent: try! NSString(contentsOfFile: NSString(string:"/Users/sergio/Documents/patterview/entrada.txt") as String, encoding: NSUTF8StringEncoding))
    
    override init(frame:CGRect) {
        super.init(frame:frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }

    func getData(widthOrig:CGFloat){
        
        let margin: CGFloat = 20.0
        let width = widthOrig - 2.0 * margin
        var acc = 0.0
        self.backgroundColor = UIColor(red: 0xF7 / 0xFF, green: 0xF7 / 0xFF, blue: 0xF7 / 0xFF, alpha: 1.0)
        
        // Reading each note
        for elem in SinglePattern.chords{
            let elemColumn = elem.dur*8.0
            
            // Creating column for note
            for _ in 0...(Int(elemColumn) == 0 ? 1 : Int(elemColumn)-1){
                let column = UIView(frame: CGRectZero)
                
                let x = CGFloat(acc*SinglePattern.widthNote())*width
                let w = CGFloat(SinglePattern.widthNote())*width
                let h = 10.0*CGFloat(SinglePattern.cantLevels()+1)
                
                //column.backgroundColor = UIColor(red: 0xF7 / 0xFF, green: 0xF7 / 0xFF, blue: 0xF7 / 0xFF, alpha: 0.5)
                column.frame = CGRect(x: x, y: 0, width: w, height: h)
                
                self.addSubview(column)

                // Creating row for each level in a note
                for row in elem.notes{
                    
                    let r = UIView(frame: CGRectZero)
                    let y = 10.0*CGFloat(abs(row - SinglePattern.maxlevel()))
                    let w2 = CGFloat(SinglePattern.widthNote())*width
                    
                    r.frame = CGRect(x: 0, y: y, width: w2 , height: 10.0)
                    
                    setUpColor(elemColumn, r: r)
                    
                    column.addSubview(r)
                } // for row
                acc += 1.0
            } // for _
        } // for note
        
        //self.addSubview(PatternFrame)
    }
    
    func cantLevels() -> Int{
        return SinglePattern.cantLevels()
    }
    
    func setUpColor(duration: Double,r : UIView){
        switch duration{
            //        case 0.5: r.backgroundColor = UIColor(red: 0xFF / 0xFF, green: 0x3b / 0xFF, blue: 0x30 / 0xFF, alpha: 0xFF / 0xFF)
            //        case 1.0: r.backgroundColor = UIColor(red: 0x1a / 0xFF, green: 0xd6 / 0xFF, blue: 0xfd / 0xFF, alpha: 0xFF / 0xFF)
            //        case 2.0: r.backgroundColor = UIColor(red: 0x87 / 0xFF, green: 0xfc / 0xFF, blue: 0x70 / 0xFF, alpha: 0xFF / 0xFF)
            //        case 4.0: r.backgroundColor = UIColor(red: 0xc6 / 0xFF, green: 0x44 / 0xFF, blue: 0xfc / 0xFF, alpha: 0xFF / 0xFF)
            //        case 8.0: r.backgroundColor = UIColor(red: 0xFF / 0xFF, green: 0xdb / 0xFF, blue: 0x4c / 0xFF, alpha: 0xFF / 0xFF)
        case 0.5: r.backgroundColor = UIColor.purpleColor()
        case 1.0: r.backgroundColor = UIColor.greenColor()
        case 2.0: r.backgroundColor = UIColor.yellowColor()
        case 4.0: r.backgroundColor = UIColor.blueColor()
        case 8.0: r.backgroundColor = UIColor.brownColor()
        default: r.backgroundColor = UIColor.blackColor()
        }
    }
}

class Element{
    var dur : Double!
    var notes : [Int]!
    
    init (dur: Double,notes:[Int]){
        self.dur = dur
        self.notes = notes
    }
}

class Pattern{
    var chords: [Element]
    
    init(fileContent: NSString){
        // duration of a note
        var lvl: [Int] = []
        var allDur: Double = 0
        var maxLevel = 0
        var minLevel = 0
        
        self.chords = []
        //Tokenizing file in single lines
        let separators = NSCharacterSet(charactersInString: "\n")
        let chords = fileContent.componentsSeparatedByCharactersInSet(separators)
        
        for chord in chords{
            //Tokenizing lines
            let separators2 = NSCharacterSet(charactersInString: " ")
            let item = chord.componentsSeparatedByCharactersInSet(separators2)
            
            allDur = 0
            lvl = []
            
            // Switching to get the duration
            for s in item{
                switch s{
                case "wn": allDur += 1
                case "hn": allDur += 1/2
                case "qn": allDur += 1/4
                case "en": allDur += 1/8
                case "sn": allDur += 1/16
                default:   if let y = Int(s) {
                    if -57<y && y < 57{
                        lvl += [y]
                        if y >= maxLevel { maxLevel = y } else if y < minLevel { minLevel = y } // if else
                    } // if &&
                    } // if let
                } // switch
            } // for
            
            
            let singleNote = Element(dur: allDur,notes: lvl)
            self.chords += [singleNote]
        }
    }
    
    func cantLevels()-> Int{
        var max = 0
        var min = 0
        for elem in chords{
            for l in elem.notes{
                if Int(l) > max{
                    max  = Int(l)
                }else if Int(l) < min{
                    min = Int(l)
                }
            }
        }
        return abs(max)+abs(min)+1
    }
    
    func cantColumns() -> Int{
        var col = 1
        for elem in chords{
            col += Int(round(elem.dur*8))
        }
        return col
    }
    
    func maxlevel() -> Int{
        var max = 0
        for elem in chords{
            for l in elem.notes{
                max = max < l ? l : max
            }
        }
        return max
    }
    
    func widthNote() -> Double{
        let w = 1/Double(self.cantColumns())
        return w
    }
}
