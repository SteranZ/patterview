//
//  ViewController.swift
//  pentagrama
//
//  Created by Sergio Teran on 5/28/16.
//  Copyright © 2016 Sergio Teran. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    // Load the Pattern
    let SinglePattern = Pattern(fileContent: try! NSString(contentsOfFile: NSString(string:"/Users/sergio/Documents/patterview/entrada.txt") as String, encoding: NSUTF8StringEncoding))
    

    //Create Frame
    let PatternFrame = UIView(frame: CGRectZero)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let margin: CGFloat = 20.0
        let width = (view.bounds.width - 2.0 * margin)
        var acc = 0.0
        PatternFrame.backgroundColor = UIColor(red: 0xF7 / 0xFF, green: 0xF7 / 0xFF, blue: 0xF7 / 0xFF, alpha: 1.0)
        
        // Reading each note
        for elem in SinglePattern.chords{
            let elemColumn = elem.dur*8.0
            
            // Creating column for note
            for _ in 0...(Int(elemColumn) == 0 ? 1 : Int(elemColumn)-1){
                let column = UIView(frame: CGRectZero)
                
                let x = CGFloat(acc*SinglePattern.widthNote())*width
                let w = CGFloat(SinglePattern.widthNote())*width
                let h = 10.0*CGFloat(SinglePattern.cantLevels())
                
                //column.backgroundColor = UIColor(red: 0xF7 / 0xFF, green: 0xF7 / 0xFF, blue: 0xF7 / 0xFF, alpha: 1.0)
                column.frame = CGRect(x: x, y: 0, width: w, height: h)
                
                PatternFrame.addSubview(column)
                
                // Creating row for each level in a note
                for row in elem.notes{
                    
                    let r = UIView(frame: CGRectZero)
                    let y = 10.0*CGFloat(abs(row - SinglePattern.maxlevel()))
                    let w2 = CGFloat(SinglePattern.widthNote())*width
                    
                    r.frame = CGRect(x: 0, y: y, width: w2 , height: 10.0)
                    
                    setUpColor(elemColumn, r: r)
                    
                    column.addSubview(r)
                } // for row
                acc += 1.0
            } // for _
        } // for note
        
        view.addSubview(PatternFrame)
    }
    
    
    func setUpColor(duration: Double,r : UIView){
        switch duration{
        case 0.5: r.backgroundColor = UIColor(red: 0xFF / 0xFF, green: 0x3b / 0xFF, blue: 0x30 / 0xFF, alpha: 0xFF / 0xFF)
        case 1.0: r.backgroundColor = UIColor(red: 0x1a / 0xFF, green: 0xd6 / 0xFF, blue: 0xfd / 0xFF, alpha: 0xFF / 0xFF)
        case 2.0: r.backgroundColor = UIColor(red: 0x87 / 0xFF, green: 0xfc / 0xFF, blue: 0x70 / 0xFF, alpha: 0xFF / 0xFF)
        case 4.0: r.backgroundColor = UIColor(red: 0xc6 / 0xFF, green: 0x44 / 0xFF, blue: 0xfc / 0xFF, alpha: 0xFF / 0xFF)
        case 8.0: r.backgroundColor = UIColor(red: 0xFF / 0xFF, green: 0xdb / 0xFF, blue: 0x4c / 0xFF, alpha: 0xFF / 0xFF)
            //        case 0.5: r.backgroundColor = UIColor.purpleColor()
            //        case 1.0: r.backgroundColor = UIColor.greenColor()
            //        case 2.0: r.backgroundColor = UIColor.yellowColor()
            //        case 4.0: r.backgroundColor = UIColor.blueColor()
            //        case 8.0: r.backgroundColor = UIColor.brownColor()
        default: r.backgroundColor = UIColor.blackColor()
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        let margin: CGFloat = 20.0
        let width = view.bounds.width - 2.0 * margin
        let height = 10.0*CGFloat(SinglePattern.cantLevels())
        
        PatternFrame.frame = CGRect( x: margin,
            y: margin + topLayoutGuide.length,
            width: width,
            height: height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Dispose of any resources that can be recreated.
    }
    
    
    
}

